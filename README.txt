README.txt
==========

A module providing a reminder on the Maintenance mode page to disable 
monitoring by the Binary Canary service. A convenient links is provided 
to the Binary Canary website login page.

To remove the notice simply disable the module. This module creates no 
fields of its own.

COMPATIBILITY NOTES
==================
- No known incompatibilities

AUTHOR/MAINTAINER
======================
-ron golan <ron AT rgon DOT us>
